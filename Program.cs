﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BT_LinQ
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student() { Name = "bang", Age = 12, Id = 122};
            Student student2 = new Student() { Name = "long", Age = 12, Id = 123 };
            Student student3 = new Student() { Name = "vinh", Age = 12, Id = 124 };
            Student student4 = new Student() { Name = "dinh", Age = 12, Id = 128 };

            IList<Student> students = new List<Student>() { student1, student2, student3, student4};

            StudentClass studentClass1 = new StudentClass() { Name = "B1" , ID = 123 };
            StudentClass studentClass2 = new StudentClass() { Name = "B2", ID = 122 };
            StudentClass studentClass3 = new StudentClass() { Name = "B6", ID = 124 };
            StudentClass studentClass4 = new StudentClass() { Name = "B7", ID = 123 };

            IList<StudentClass> studentClasses = new List<StudentClass>() { studentClass1, studentClass2, studentClass3 };

            var joinClass = students.Join(
                studentClasses,
                student => student.Id,
                studentClasse => studentClasse.ID,
                (student, classSt) => new
                {
                    StudentName = student.Name,
                    ClassofStudent = classSt.Name,
                });
            foreach (var item in joinClass) {
                Console.WriteLine(item.StudentName + " " + item.ClassofStudent);
            }
            Console.ReadLine();

        }
    }
}
